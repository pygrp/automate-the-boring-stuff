#! python3
# An insecure password locker

passwords = {'email':'fasd34rffff', 'aws':'arg435rve34rf', 'azure':'23453ferce3wdc'}

import sys, pyperclip

if len(sys.argv) < 2:
    print('Enter account name - ')
    sys.exit()

account = sys.argv[1]

if account in passwords:
    print (passwords[account])
    pyperclip.copy (passwords[account])
    
