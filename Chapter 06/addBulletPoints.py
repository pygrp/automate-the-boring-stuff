#! python3

# Add bullet points in front of each lines.
import pyperclip

text = pyperclip.paste()
lines = text.split('\n')

for i in range(len(lines)):
    lines[i] = '* ' + lines[i]
    
text = '\n'.join(lines)
pyperclip.copy(text)


