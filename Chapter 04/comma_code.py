import logging

logging.basicConfig(level=logging.DEBUG, format = '%(asctime)s - %(levelname)s - %(message)s')
logging.disable()

spam = ['apples','bananas','tofu','cats','animal']

def listSep(aList):
    ans=""
    length = len(aList)
    for i in range(length-1):
        ans = ans + aList[i] +', '       
    ans = ans + 'and ' + aList[-1]
    print (ans)

listSep(spam)
