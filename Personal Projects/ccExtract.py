# This file takes bank statement table data in text format and creates a CSV file.

import logging
import csv
import sys
import os

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s - %(levelname)s - %(message)s')
logging.disable()

ccFile = open('C:\\Users\\bhave\\source\\repos\\starworldx\\automate-the-boring-stuff\\Personal Projects\\ccExtract.txt', 'r')
content = ccFile.read()
ccFile.close()

lines = content.splitlines()
outputFile = open('ccExtractOutput.csv', 'w', newline='')

for line in lines:
    lineItems = line.split()

    length = len(lineItems)
    if length < 5:  # move on to next line if there are not five columns of data in the line.
        continue

    try:  # remove thousand separator from the amount and convert to float.
        x = ""
        for char in lineItems[-1]:
            if char == ',':
                continue
            x = x + char
        lineItems[-1] = float(x)
    except ValueError as e:
        print(e)
        # if trying convert a comment to float, ignore the error and move to next line.
        continue

    # Find Amount from end of the line.
    if lineItems[-2] == "-":
        amount = float((lineItems[length-2]) + str(lineItems[-1]))
    else:
        amount = (lineItems[-1])
    lineItems.insert(0, amount)  # adds amount in the first column.

    desc = ""  # Transaction description text concatenation.
    for items in lineItems[2:6]:
        desc = desc + " " + items
    lineItems.insert(3, desc)  # adds description in fourth column.

    # write lines to a CSV file.
    outputWriter = csv.writer(outputFile)
    outputWriter.writerow(lineItems[0:4])

outputFile.close()
print(f"Output file can be found at {os.getcwd()}\\{outputFile.name}")
sys.exit()  # produces error in VS community edition.
