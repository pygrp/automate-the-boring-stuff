import random
import bpio
import logging
logging.basicConfig(filename='logfile.txt', level=logging.DEBUG, format = '%(asctime)s - %(levelname)s - %(message)s')
logging.disable()


print("Enter # of times you would like to guess?")
maxTries = bpio.intInput()
minTries = 0
logging.info('Max Tries = '+ str(maxTries)+ '\n')


# lower, upper = bpio.multiInput(2, int, ",")
lower, upper = input("Enter upper and lower limit as integer for a random number, separated by a space.\n ").split()
lower = int(lower)
upper = int(upper)

userNum = 0

randNum = random.randint(lower,upper)

for tryTaken in range(minTries, maxTries):
    while True:
        print("Enter your guess between {} & {}".format(lower, upper))
        userNum = bpio.intInput()

        if (userNum < lower or userNum > upper):
            print("Guess within defined range of {} & {}".format(lower, upper))
            continue
        else:
            break
    
    if (userNum < randNum and userNum >= lower):
        print(f'You guessed low. Try again. You have {((maxTries - tryTaken) - 1)} tries left.')
    elif (userNum > randNum and userNum < upper):
        print("You guess High. Try again. You have {} tries left.".format((maxTries - tryTaken) - 1))
    else:
        print(f"You guessed correct. It took you {tryTaken+1} tries.")
        break

print("End of Program.")