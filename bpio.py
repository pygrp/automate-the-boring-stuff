# This module is for input and output formatting.
# bp prefix is initial of my first and last name.

import traceback

def intInput():
    while True:
        try:
            print("Print now >> ")
            x = int(input())
            break
        except ValueError as e:
            print("Your input generated following error: ", e)
            errorFile = open('errorinfo.txt','a')
            errorFile.write(traceback.format_exc()+'\n')
            errorFile.close()
            continue 
    return x

def multiInput(varCount, inputType, splitChar):
    while True:
        try:
            print("Print now >> ")
            x = int(input())
            break
        except ValueError as e:
            print("Your input generated following error: ", e)
            continue 
    return x