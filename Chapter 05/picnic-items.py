allGuests = {'Alice': {'Apples': 2, 'Bananas':3}, 
             'Bob': {'Oranges': 1, 'Apples': 1},
             'Charlie': {'Mangoes': 1, 'Bananas':1}
             }

def total(dataset, fruit):
    brought = 0
    for k, v in dataset.items():
        brought = brought + v.get(fruit,0)
    return brought

print('Total Apples brought = ' + str(total(allGuests, 'Apples')))
