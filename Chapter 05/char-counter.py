import pprint

m = "This is a test message. For illustration purpose only."
counter = {}

for char in m:
    counter.setdefault(char, 0)
    counter[char] = counter[char]+1
    
pprint.pprint(counter)